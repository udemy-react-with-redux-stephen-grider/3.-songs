import React from 'react'
import { connect } from 'react-redux';

const SongDetail = ({ song }) => {
  if (song) {
    return (
      <div>
        <h3>Thong tin:</h3>
        <p>
          Title:  {song.title}
        </p>
      </div>
    )
  }
  return <div>No song</div>
}

const mapStateToProp = state => {
  return { song: state.selectedSong }
}

export default connect(mapStateToProp)(SongDetail);