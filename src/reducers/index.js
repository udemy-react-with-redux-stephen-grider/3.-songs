import { combineReducers } from 'redux';

const songsReducer = () => {
  return [
    { title: 'mot', id: 1, duration: '4:33' },
    { title: 'hai', id: 2, duration: '2:59' },
    { title: 'ba', id: 3, duration: '3:21' },
  ]
};

const selectedSongReducer = (selectedSong = null, action) => {
  if (action.type === 'SONG_SELECTED') {
    return action.payload;
  }
  return selectedSong;
}

export default combineReducers({
  songs: songsReducer,
  selectedSong: selectedSongReducer
})